package com.example.surinderkahlon.assignmenta;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    TextView textView;
    Button button;
    EditText editText1,editText2;
    public ArrayList<Double> milesArrayList = new ArrayList<Double>();
    public ArrayList<Double> gallonsArrayList = new ArrayList<Double>();
    public ArrayList<Double> mpgArrayList = new ArrayList<Double>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = (TextView)findViewById(R.id.textView);
        editText1 = (EditText)findViewById(R.id.editText);
        editText2 = (EditText)findViewById(R.id.editText2);
        button = (Button) findViewById(R.id.button2);
        if(mpgArrayList.size()==0){
            button.setEnabled(false);
        }


    }
    public void getMPG(View view){
        Double miles = Double.valueOf(editText1.getText().toString());
        milesArrayList.add(miles);

        Double gallons = Double.valueOf(editText2.getText().toString());
        gallonsArrayList.add(gallons);
        Double mpg = miles/gallons;
        CalculationHistory calculationHistory = new CalculationHistory();
        mpgArrayList.add(mpg);
        textView.setText(mpg.toString()+" Miles per Gallons");
        System.out.println(milesArrayList.get(0)+"aaaaaaaaassssss");
        if(mpgArrayList.size()>=1){
            button.setEnabled(true);
        }
      //  System.out.println(milesArrayList.get(1)+"aaaaaaaaassssss");
    }
    public void goToHistory(View view) {


        Intent intent = new Intent(MainActivity.this, CalculationHistory.class);
        Bundle args = new Bundle();
        Bundle args2 = new Bundle();
        Bundle args3 = new Bundle();

        args.putSerializable("ARRAYLIST",(Serializable)milesArrayList);
        intent.putExtra("BUNDLE",args);
        args2.putSerializable("ARRAYLIST2",(Serializable)gallonsArrayList);
        intent.putExtra("BUNDLE2",args2);
        args3.putSerializable("ARRAYLIST3",(Serializable)mpgArrayList);
        intent.putExtra("BUNDLE3",args3);
        startActivity(intent);

    }
}
